# !/usr/bin/python3
# -*- coding: utf-8 -*-

__version__ = "1.0"
__author__ = "Rachid Azizi"
__credits__ = ["Rachid Aizi"]
__license__ = "Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>"
__maintainer__ = ["Rachid Azizi"]
__email__ = ["azi92rach@gmail.com"]
__status__ = "Development"

from mccdaqR.usb1608GX import DaqR_usb1608G