from mccdaqR import DaqR_usb1608G
import matplotlib.pyplot as plt, numpy as np
from scipy.fftpack import fft


if __name__ == '__main__':

    # Create the object daq
    daq = DaqR_usb1608G()

    # Print information
    print('wMaxPacketSize:', daq.getMaxPacketSize())

    # Parameters
    c = 0 # channel
    gain = 10 # in Volt. 10V, 5V, 2V, 1V
    mode = 0 # 0 for single endian, 1 for differential
    freq = 500000 # Hz
    t = 1 # seconds

    # Scanner the chennel
    print('Scan...')
    vecOut, vecTime = daq.scan(channel=c, g=gain, m=mode, timeScan=t, frequency=freq)
    print('End scan. Plot..')

    # Fourier Transform
    N = len(vecOut)
    yf = fft(vecOut)

    # Plot results
    plt.figure(1)
    plt.subplot(211)
    plt.plot(vecTime, vecOut)
    plt.xlabel('Time')
    plt.ylabel('Volt')
    plt.title('Example USB-1608G')

    plt.subplot(212)
    plt.plot(2.0/N * np.abs(yf[0:N//2]))
    plt.xlabel('Frequency')
    plt.ylabel('Module')
    plt.title('FFT')
    plt.show()






